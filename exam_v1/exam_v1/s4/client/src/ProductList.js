import React, { Component } from 'react';

class ProductList extends Component {
    constructor(props){
        super(props);
        this.state = {
            products: []
        }
    }
    
    componentDidMount(){
        fetch('/get-all').then(res => {
            this.setState({
                products:res.json()
            })
        })
        console.log("STATE", this.state)
    }
    
    render(){
        return (

            <div>
            
                {this.state.products.map( (product, index) =>
                <ul>
                    Product #{index}
                    <li>Product ID: {product.id}</li>
                    <li>Product Name: {product.productName}</li>
                    <li>Product Price: {product.price}</li>
                </ul>
                )}
            
            </div>

            )
    }
}

export default ProductList;