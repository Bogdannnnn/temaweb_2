import React, { Component } from 'react';

class AddProduct extends Component {
    constructor(props){
        super(props);
        this.state={
            price:'',
            name: ''
        }
    }
    
    
    updatePrice = (event) => {
        this.setState({
            price: event.target.value
        })
        console.log("State after updatePrice", this.state)
    }
    
    updateName = (event) => {
        this.setState({
            name: event.target.value
        })
        
        console.log("State after updateName", this.state)
    }
    
    update = () => {
        var product = {};
        product.productName = this.state.name;
        product.price = this.state.price;
        
        fetch('/add', {
                method: 'post',
                body: JSON.stringify(product),
                headers:{
                    'Content-Type': 'application/json'
                  }
    })};
    
    render(){
        return (
            <div>
            <form>
              <label>
                Product price:
                <input type="text" name="price" onChange={this.updatePrice}/>
              </label>
              <label>
                Product name:
                <input type="text" name="name" onChange={this.updateName}/>
              </label>
              <input type="submit" value="Submit" onClick={this.update}/>
            </form>
            </div>
            )
    }
}

export default AddProduct;